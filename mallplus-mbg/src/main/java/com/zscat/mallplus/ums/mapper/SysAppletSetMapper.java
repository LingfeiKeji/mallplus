package com.zscat.mallplus.ums.mapper;

import com.zscat.mallplus.ums.entity.SysAppletSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2019-06-15
 */
public interface SysAppletSetMapper extends BaseMapper<SysAppletSet> {

}
