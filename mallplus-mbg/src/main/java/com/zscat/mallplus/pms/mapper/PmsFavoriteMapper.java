package com.zscat.mallplus.pms.mapper;

import com.zscat.mallplus.pms.entity.PmsFavorite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2019-06-15
 */
public interface PmsFavoriteMapper extends BaseMapper<PmsFavorite> {

}
