package com.zscat.mallplus.pms.mapper;

import com.zscat.mallplus.pms.entity.PmsGiftsCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 帮助分类表 Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2019-07-07
 */
public interface PmsGiftsCategoryMapper extends BaseMapper<PmsGiftsCategory> {

}
