package com.zscat.mallplus.pms.mapper;

import com.zscat.mallplus.pms.entity.PmsGifts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 帮助表 Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2019-07-07
 */
public interface PmsGiftsMapper extends BaseMapper<PmsGifts> {

}
